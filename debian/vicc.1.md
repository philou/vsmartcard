% VICC(1) VICC Man Page
% Philippe Thierry
% June 2020
# NAME
vicc - a Virtual Smart Card emulator written in Python

# SYNPSIS
**vicc [-h] [-t {iso7816,cryptoflex,ePass,nPA,relay,handler\_test}] [-v]**
**[-f FILE] [-H HOSTNAME] [-P PORT] [-R] [--version]**
**[--reader READER] [--ef-cardaccess EF_CARDACCESS]**
**[--ef-cardsecurity E\_CARDSECURITY] [--cvca CVCA]**
**[--disable-ta-checks] [--ca-key CA_KEY] [-d DATASETFILE]**
**[--esign-cert ESIGN\_CERT] [--esign-ca-cert ESIGN_CA_CERT]**

# DESCRIPTION

Virtual Smart Card 0.8: Smart card emulator written in Python. The emulator
connects to the virtual smart card reader reader (vpcd). Smart card
applications can access the Virtual Smart Card through the vpcd via PC/SC.

# OPTIONS

## General options:

**-h, --help**
  show this help message and exit

**-t {iso7816,cryptoflex,ePass,nPA,relay,handler_test}, --type {iso7816,cryptoflex,ePass,nPA,relay,handler_test}**
  type of smart card to emulate (default: iso7816)

**-v, --verbose**
  Use (several times) to be more verbose

**-f FILE, --file FILE**
  load a saved smart card image

**-H HOSTNAME, --hostname HOSTNAME**
  specify vpcd's host name if vicc shall connect to it. (default: localhost)

**-P PORT, --port PORT**
  port of connection establishment (default: 35963)

**-R, --reversed**
  use reversed connection mode. vicc will wait for an incoming connection from vpcd. (default: False)

**--version**
  show program's version number and exit

## Relaying a local smart card options (`--type=relay`):

**--reader READER**
  number of the reader containing the card to be relayed (default: 0)

## Emulation of German identity card (`--type=nPA`):

**--ef-cardaccess EF_CARDACCESS**
  the card's EF.CardAccess (default: use file from first generation nPA)

**--ef-cardsecurity EF_CARDSECURITY**
  the card's EF.CardSecurity (default: use file from first generation nPA)

**--cvca CVCA**
  trust anchor for verifying certificates in TA (default: use libeac's trusted certificates)

**--disable-ta-checks**
  disable checking the validity period of CV certificates (default: False)

**--ca-key CA_KEY**
  the chip's private key for CA (default: randomly generated, invalidates signature of EF.CardSecurity)

**-d DATASETFILE, --datasetfile DATASETFILE**
  Load the smartcard's data groups (DGs) from the specified dataset file. For DGs not
in dataset file default values are used. The data groups in the data set file must have
the following syntax:

```
Datagroupname=Datagroupvalue
```

For Example: GivenNames=GERTRUD. The following Dataset Elements may be used in the
dataset file:
   * DocumentType
   * IssuingState
   * DateOfExpiry
   * GivenNames
   * FamilyNames
   * ReligiousArtisticName
   * AcademicTitle
   * DateOfBirth
   * PlaceOfBirth
   *  Nationality
   * Sex
   * BirthName
   * Country
   * City
   * ZIP
   * Street
   * CommunityID
   * ResidencePermit1
   * ResidencePermit2
   * dg12
   * dg14
   * dg15
   * dg16
   * dg21.

**--esign-cert ESIGN_CERT**
  the card holder's certificate for QES

**--esign-ca-cert ESIGN_CA_CERT**
  the CA's certificate for QES

Report bugs to https://github.com/frankmorgner/vsmartcard/issues

# HISTORY

June 2020, Man page originally compiled by Philippe Thierry (philou at debian dot org)

