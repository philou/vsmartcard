#!/bin/sh

set -x

autoreconf -vif ${VIRTUALSMARTCARD_DIR}
autoreconf -vif ${PSCSRELAY_DIR}
